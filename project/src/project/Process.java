package project;

import Jama.Matrix;
import ij.ImagePlus;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;

public class Process implements Initializable {

    private ImagePlus i;
    private ImagePlus p;

    private ColorTransform iCt;
    private ColorTransform pCt;

	public static final int Y = 4;
	public static final int Cb = 5;
	public static final int Cr = 6;

	public static final int B2 = 2;
	public static final int B4 = 4;
	public static final int B8 = 8;
	public static final int B16 = 16;

	public int blocksType = B2;
	public static final int macroblockSize = 16;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
    }
	
	public Process()
	{
		reloadResources();
//        this.i.show();
//        this.p.show();
	}

    private void movementPrediction() {
        iCt.RGBtoYCbCr();
        pCt.RGBtoYCbCr();

        // temporary ColorTransform 1 & 2
        ColorTransform tempColorTransform = new ColorTransform(i.getBufferedImage());
        tempColorTransform.getRGB();
        tempColorTransform.RGBtoYCbCr();

        ColorTransform temp2ColorTransform = new ColorTransform(p.getBufferedImage());
        temp2ColorTransform.getRGB();
        temp2ColorTransform.RGBtoYCbCr();

        // extend matrix
        Matrix extended = extendMatrix(pCt.getY());

        double sad = 0;

        // through search window
        for (int m = 0; m < tempColorTransform.getWidth(); m=m+macroblockSize) {
            for (int n = 0; n < tempColorTransform.getHeight(); n=n+macroblockSize) {

                Matrix macroblock = getMacroblock(m, n);

                double minMae = Integer.MAX_VALUE;
                int deltaX = 0;
                int deltaY = 0;

                for (int k = 0; k < macroblockSize; k++) {
                    for (int l = 0; l < macroblockSize; l++) {

                        double singleMae = 0;
                        for (int i = 0; i < macroblock.getColumnDimension(); i++) {
                            for (int j = 0; j < macroblock.getRowDimension(); j++) {
                                singleMae += Math.abs(extended.get(i + k, j + l) - macroblock.get(i, j));
                            }
                        }
                        singleMae /= macroblockSize * macroblockSize;
                        if (singleMae < minMae) {
                            minMae = singleMae;
                            deltaX = k - (macroblockSize/2);
                            deltaY = l - (macroblockSize/2);
                        }
                    }
                }

                sad += minMae;

                // movement vectors & prediction error MAE
//                System.out.println("MAE: " + minMae);
//                System.out.println("dX: " + deltaX);
//                System.out.println("dY: " + deltaY);

                extended.get(m + (macroblockSize/2), n + (macroblockSize/2));

                // movement compensation
                for (int i = 0; i < macroblockSize; i++) {
                    for (int j = 0; j < macroblockSize; j++) {
                        if (m+i+deltaX > 0 && m+i+deltaX < 240 && n+j+deltaY < 240 && n+j+deltaY > 0) {
                            // Y
                            double bufferY = tempColorTransform.getY().get(m + i, n + j);
                            tempColorTransform.getY().set(m+i+deltaX, n+j+deltaY, bufferY);
                            temp2ColorTransform.getY().set(m+i+deltaX, n+j+deltaY, bufferY);
                            // Cb
                            double bufferCb = tempColorTransform.getcB().get(m + i, n + j);
                            tempColorTransform.getcB().set(m+i+deltaX, n+j+deltaY, bufferCb);
                            temp2ColorTransform.getcB().set(m+i+deltaX, n+j+deltaY, bufferCb);
                            // Cr
                            double bufferCr = tempColorTransform.getcR().get(m + i, n + j);
                            tempColorTransform.getcR().set(m+i+deltaX, n+j+deltaY, bufferCr);
                            temp2ColorTransform.getcB().set(m+i+deltaX, n+j+deltaY, bufferCb);
                        }
                    }
                }
            }
        }

        System.out.println("SAD: " + sad);

        Matrix iMatrixY = tempColorTransform.getY();
        Matrix iMatrixCb = tempColorTransform.getcB();
        Matrix iMatrixCr = tempColorTransform.getcR();

        Matrix pMatrixY = temp2ColorTransform.getY();
        Matrix pMatrixCb = temp2ColorTransform.getcB();
        Matrix pMatrixCr = temp2ColorTransform.getcR();

        Matrix tempY = new Matrix(iMatrixY.getColumnDimension(), iMatrixY.getRowDimension());
        Matrix tempY2 = new Matrix(pMatrixY.getColumnDimension(), pMatrixY.getRowDimension());

        Matrix tempCb = new Matrix(iMatrixY.getColumnDimension(), iMatrixY.getRowDimension());
        Matrix tempCb2 = new Matrix(pMatrixY.getColumnDimension(), pMatrixY.getRowDimension());

        Matrix tempCr = new Matrix(iMatrixY.getColumnDimension(), iMatrixY.getRowDimension());
        Matrix tempCr2 = new Matrix(pMatrixY.getColumnDimension(), pMatrixY.getRowDimension());

        for (int i = 0; i < iMatrixY.getColumnDimension(); i++) {
            for (int j = 0; j < iMatrixY.getRowDimension(); j++) {
                double bufferY = (pCt.getY().get(i, j)+255-iMatrixY.get(i, j)) / 2;
                double bufferY2 = (iCt.getY().get(i, j)+255-pMatrixY.get(i, j)) / 2;
                tempY.set(i, j, bufferY);
                tempY2.set(i, j, bufferY2);

                double bufferCb = (pCt.getcB().get(i, j)+255-iMatrixCb.get(i, j)) / 2;
                double bufferCb2 = (iCt.getcB().get(i, j)+255-pMatrixCb.get(i, j)) / 2;
                tempCb.set(i, j, bufferCb);
                tempCb2.set(i, j, bufferCb2);

                double bufferCr = (pCt.getcR().get(i, j)+255-iMatrixCr.get(i, j)) / 2;
                double bufferCr2 = (iCt.getcR().get(i, j)+255-pMatrixCr.get(i, j)) / 2;
                tempCr.set(i, j, bufferCr);
                tempCr2.set(i, j, bufferCr2);
            }
        }
        // Y
        tempColorTransform.setY(tempY);
        tempColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getY(), "Y1").show();

        temp2ColorTransform.setY(tempY2);
        temp2ColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getY(), "Y2").show();

        // Cb
        tempColorTransform.setcB(tempCb);
        tempColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getcB(), "Cb1").show();

        temp2ColorTransform.setcB(tempCb2);
        temp2ColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getcB(), "Cb2").show();

        // Cr
        tempColorTransform.setcR(tempCr);
        tempColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getcR(), "Cr1").show();

        temp2ColorTransform.setcR(tempCr2);
        temp2ColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getcR(), "Cr2").show();

        // DPCM po kompenzaci
        dpcm(tempColorTransform, temp2ColorTransform);
	}
	
	private void reloadResources()
	{
        this.i = new ImagePlus("pomaly.jpg");
        this.p = new ImagePlus("pomaly2.jpg");

        this.iCt = new ColorTransform(this.i.getBufferedImage());
        this.pCt = new ColorTransform(this.p.getBufferedImage());

        iCt.getRGB();
        pCt.getRGB();
	}

	public Slider quality;

    public void rgb2ycbcrButtonPressed() {
        iCt.RGBtoYCbCr();
        pCt.RGBtoYCbCr();
    }

    public void ycbcr2rgbButtonPressed() {
        iCt.YCbCrtoRGB();
        pCt.YCbCrtoRGB();
    }

    public void y1ButtonPressed() { getComponentC(Y).show(); }
	public void y2ButtonPressed() { getComponentP(Y).show(); }
    public void cb1ButtonPressed() { getComponentC(Cb).show(); }
    public void cb2ButtonPressed() { getComponentP(Cb).show(); }
    public void cr1ButtonPressed() { getComponentC(Cr).show(); }
    public void cr2ButtonPressed() { getComponentP(Cr).show(); }

    public Button dctButton;
    public Button idctButton;

    public void dctButtonPressed(ActionEvent event) {
        dct();
        dctButton.setDisable(true);
        idctButton.setDisable(false);
    }

    public void idctButtonPressed(ActionEvent event) {
        idct();
        idctButton.setDisable(true);
        dctButton.setDisable(false);
    }

    public void dpcmButtonPressed() {
        ColorTransform i = new ColorTransform(this.i.getBufferedImage());
        ColorTransform p = new ColorTransform(this.p.getBufferedImage());
        dpcm(i, p);
    }

    public void fullSearchButtonPressed() { movementPrediction(); }

    public void dpcm(ColorTransform i, ColorTransform p) {
        i.getRGB();
        i.RGBtoYCbCr();

        Matrix iMatrixY = i.getY();
        Matrix iMatrixCb = i.getcB();
        Matrix iMatrixCr = i.getcR();

        p.getRGB();
        p.RGBtoYCbCr();

        Matrix pMatrixY = p.getY();
        Matrix pMatrixCb = p.getcR();
        Matrix pMatrixCr = p.getcR();

        Matrix tempY = new Matrix(iMatrixY.getColumnDimension(), iMatrixY.getRowDimension());
        Matrix tempCb = new Matrix(iMatrixCb.getColumnDimension(), iMatrixCb.getRowDimension());
        Matrix tempCr = new Matrix(iMatrixCr.getColumnDimension(), iMatrixCr.getRowDimension());

        for (int k = 0; k < iMatrixY.getColumnDimension(); k++) {
            for (int j = 0; j < iMatrixY.getRowDimension(); j++) {
                double bufferY = ( (pMatrixY.get(k, j)+255)-(iMatrixY.get(k, j)) ) / 2;
                tempY.set(k, j, bufferY);
                double bufferCb = ( (pMatrixCb.get(k, j)+255)-(iMatrixCb.get(k, j)) ) / 2;
                tempCb.set(k, j, bufferCb);
                double bufferCr = ( (pMatrixCr.get(k, j)+255)-(iMatrixCr.get(k, j)) ) /2;
                tempCr.set(k, j, bufferCr);
            }
        }

        ColorTransform tempColorTransform = new ColorTransform(this.i.getBufferedImage());
        tempColorTransform.setY(tempY);
        tempColorTransform.setcB(tempCb);
        tempColorTransform.setcR(tempCr);
        tempColorTransform.YCbCrtoRGB();
        tempColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getY(), "Y - DPCM").show();
        tempColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getcB(), "Cb - DPCM").show();
        tempColorTransform.setImageFromRGB(tempColorTransform.getHeight(), tempColorTransform.getWidth(), tempColorTransform.getcR(), "Cr - DPCM").show();
    }

    public Matrix getMacroblock(int x, int y) {
        Matrix output = iCt.getY().getMatrix(x, x+macroblockSize-1, y, y+macroblockSize-1);
//        for (int i = 0; i < 16; i++) {
//            for (int j = 0; j < 16; j++) {
//                if (output.get(i, j) > 99.99) {
//                    System.out.print(String.format("%.2f", output.get(i, j)) + " ");
//                } else {
//                    System.out.print(" " + String.format("%.2f", output.get(i, j)) + " ");
//                }
//            }
//            System.out.println();
//        }
        return output;
    }

    public Matrix extendMatrix(Matrix input) {
        Matrix output = new Matrix(input.getColumnDimension()+macroblockSize, input.getRowDimension()+macroblockSize);

        // [0-8][0-8], [25-32][0-8]
        for (int i = 0; i < macroblockSize/2; i++) {
            for (int j = 0; j < macroblockSize/2; j++) {
                output.set(i, j, input.get(0, 0));
                output.set(i, j+(macroblockSize/2)+input.getColumnDimension(), input.get(macroblockSize-1, 0));
                output.set(i+(macroblockSize/2)+input.getRowDimension(), j, input.get(0, 0));
                output.set(i+(macroblockSize/2)+input.getRowDimension(), j+(macroblockSize/2)+input.getColumnDimension(), input.get(macroblockSize-1, 0));
            }
        }

        // [9-24][0-8]
        for (int k = 0; k < input.getColumnDimension(); k++) {
            for (int l = 0; l < macroblockSize/2; l++) {
                output.set(l, k+(macroblockSize/2), input.get(0, k));
            }
        }

        // [9-24][25-32]
        for (int k = 0; k < input.getColumnDimension(); k++) {
            for (int l = input.getRowDimension()+(macroblockSize/2); l < input.getRowDimension()+macroblockSize; l++) {
                output.set(l, k+(macroblockSize/2), input.get(macroblockSize-1, k));
            }
        }

        // [0-8][8-16]
        for (int k = macroblockSize/2; k < input.getRowDimension()+(macroblockSize/2); k++) {
            for (int l = 0; l < macroblockSize/2; l++) {
                output.set(k, l, input.get(k-(macroblockSize/2), 0));
            }
        }

        // [25-32][8-16]
        for (int k = macroblockSize/2; k < input.getRowDimension()+(macroblockSize/2); k++) {
            for (int l = input.getColumnDimension()+(macroblockSize/2); l < input.getColumnDimension()+macroblockSize; l++) {
                output.set(k, l, input.get(k-(macroblockSize/2), input.getColumnDimension()-1));
            }
        }

        // original matrix
        output.setMatrix(macroblockSize/2, (macroblockSize/2)+macroblockSize-1, macroblockSize/2, (macroblockSize/2)+macroblockSize-1, input);

        // vypis
//        for (int i = 0; i < output.getColumnDimension(); i++) {
//            for (int j = 0; j < output.getRowDimension(); j++) {
//                if (output.get(i, j) > 99.99) {
//                    System.out.print(String.format("%.2f", output.get(i, j)) + " ");
//                } else if (output.get(i, j) < 9.99) {
//                    System.out.print(String.format("  " + "%.2f", output.get(i, j)) + " ");
//                } else {
//                    System.out.print(" " + String.format("%.2f", output.get(i, j)) + " ");
//                }
//            }
//            System.out.println();
//        }

        return output;
    }


    /**
     * NOT USED
     */

    public ImagePlus getComponentC(int component) {
        ImagePlus image = null;
        switch (component) {
            case Y:
                image = this.iCt.setImageFromRGB(this.iCt.getHeight(), this.iCt.getWidth(), this.iCt.getY(), "Y");
                break;
            case Cb:
                image = this.iCt.setImageFromRGB(this.iCt.getcB().getRowDimension(), this.iCt.getcB().getColumnDimension(), this.iCt.getcB(), "Cb");
                break;
            case Cr:
                image = this.iCt.setImageFromRGB(this.iCt.getcR().getRowDimension(), this.iCt.getcR().getColumnDimension(), this.iCt.getcR(), "Cr");
                break;
        }

        return image;
    }

    public ImagePlus getComponentP(int component) {
        ImagePlus image = null;
        switch (component) {
            case Y:
                image = this.pCt.setImageFromRGB(this.pCt.getHeight(), this.pCt.getWidth(), this.pCt.getY(), "Y");
                break;
            case Cb:
                image = this.pCt.setImageFromRGB(this.pCt.getcB().getRowDimension(), this.pCt.getcB().getColumnDimension(), this.pCt.getcB(), "Cb");
                break;
            case Cr:
                image = this.pCt.setImageFromRGB(this.pCt.getcR().getRowDimension(), this.pCt.getcR().getColumnDimension(), this.pCt.getcR(), "Cr");
                break;
        }

        return image;
    }

	public void dct() {
		int size = this.blocksType;
		TransformMatrix tm = new TransformMatrix();
		Matrix transformMatrix = tm.getDctMatrix(size);

		int rows = this.iCt.getY().getRowDimension();
		int cols = this.iCt.getY().getColumnDimension();

		for (int i = 0; i + size - 1 < rows; i = i + size) {
			for (int j = 0; j + size - 1 < cols; j = j + size) {
				// transform
				Matrix y = this.iCt.transform(size, transformMatrix, this.iCt.getY().getMatrix(i, i + size - 1, j, j + size - 1));
				y = quantize(y, Y);
				this.iCt.getY().setMatrix(i, i + size - 1, j, j + size - 1, y);

				Matrix cB = this.iCt.transform(size, transformMatrix, this.iCt.getcB().getMatrix(i, i+ size - 1, j,j + size - 1));
				cB = quantize(cB, Cb);
				this.iCt.getcB().setMatrix(i, i + size - 1, j,j + size - 1, cB);

				Matrix cR = this.iCt.transform(size, transformMatrix, this.iCt.getcR().getMatrix(i, i + size - 1, j,j + size - 1));
				cR = quantize(cR, Cr);
				this.iCt.getcR().setMatrix(i, i + size - 1, j,j + size - 1, cR);
			}
		}
	}

	public void idct() {
		int size = this.blocksType;
		TransformMatrix tm = new TransformMatrix();
		Matrix transformMatrix = tm.getDctMatrix(size);

		int rows = this.iCt.getY().getRowDimension();
		int cols = this.iCt.getY().getColumnDimension();

		for (int i = 0; i + size - 1 < rows; i = i + size) {
			for (int j = 0; j + size - 1 < cols; j = j + size) {
				Matrix y = this.iCt.inverseTransform(size, transformMatrix, this.iCt.getY().getMatrix(i, i + size - 1, j,j + size - 1));
				y = dequantize(y, Y);
				this.iCt.getY().setMatrix(i, i + size - 1, j, j + size - 1, y);

				Matrix cB = this.iCt.inverseTransform(size, transformMatrix, this.iCt.getcB().getMatrix(i, i + size - 1, j,j + size - 1));
				cB = dequantize(cB, Cb);
				this.iCt.getcB().setMatrix(i, i + size - 1, j,j + size - 1, cB);

				Matrix cR = this.iCt.inverseTransform(size, transformMatrix, this.iCt.getcR().getMatrix(i, i + size - 1, j,j + size - 1));
				cR = dequantize(cR, Cr);
				this.iCt.getcR().setMatrix(i, i + size - 1, j,j + size - 1, cR);
			}
		}

		// YCbCr to RGB
		this.iCt.YCbCrtoRGB();
		//getComponentC(Y).show("Y");
		//getComponentC(Cb).show("Cb");
		//getComponentC(Cr).show("Cr");

		this.i = this.iCt.setImageFromRGB(this.i.getWidth(), this.i.getHeight(), this.iCt.getRed(), this.iCt.getGreen(), this.iCt.getBlue());
		this.i.show();

	}

	public Matrix quantize(Matrix m, int type) {
		double alfa;
		if (this.quality.getValue() < 50) {
			alfa = 50 / this.quality.getValue();
		} else {
			alfa = 2 - (2*this.quality.getValue() / 100);
		}

		QuantizationMatrix qm = new QuantizationMatrix();
		Matrix q;

		if (type == Y) {
			q = qm.getYMatrix();
		} else {
			q = qm.getCMatrix();
		}

		for (int i = 0; i < m.getRowDimension(); i++) {
			for (int j = 0; j < m.getColumnDimension(); j++) {
				double result = (int)Math.floor(m.get(i, j) / (alfa * q.get(i, j)));
				m.set(i, j, result);
			}
		}

		return m;
	}

	public Matrix dequantize(Matrix m, int type) {
		double alfa;
		if (this.quality.getValue() < 50) {
			alfa = 50 / this.quality.getValue();
		} else {
			alfa = 2 - (2*this.quality.getValue() / 100);
		}

		QuantizationMatrix qm = new QuantizationMatrix();
		Matrix q;

		if (type == Y) {
			q = qm.getYMatrix();
		} else {
			q = qm.getCMatrix();
		}

		for (int i = 0; i < m.getRowDimension(); i++) {
			for (int j = 0; j < m.getColumnDimension(); j++) {
				int result = (int)(m.get(i, j) * (alfa * q.get(i, j)));
				if (result > 255) {
					result = 255;
				} else if (result < 0) {
					result = 0;
				}
				m.set(i, j, result);
			}
		}
		return m;
	}

}
