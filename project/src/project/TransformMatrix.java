package project;

import Jama.Matrix;

class TransformMatrix {

	Matrix getDctMatrix(int size)
	{
		Matrix dct = new Matrix(size, size);
		double temp;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (i == 0)
					temp = Math.sqrt((double)1/size) * Math.cos(((2.0 * j + 1.0) * i * 3.14) / (2.0 * size));
				else
					temp = Math.sqrt((double)2/size) * Math.cos(((2.0 * j + 1.0) * i * 3.14) / (2.0 * size));
				dct.set(i, j, temp);
			}
		}

		return dct;
	}
}

