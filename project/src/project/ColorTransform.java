package project;

import ij.ImagePlus;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

import Jama.Matrix;

public class ColorTransform {
	
	private BufferedImage bImage;
	private ColorModel colorModel;
	
	private int [][] red;
	private int [][] green;
	private int [][] blue;
	
	private int width;
	private int height;
	
	private Matrix y;
	private Matrix cB;
	private Matrix cR;


	public void setY(Matrix Y) {
		this.y = Y;
	}

	public void setcB(Matrix cB) {
		this.cB = cB;
	}

	public void setcR(Matrix cR) {
		this.cR = cR;
	}

	public Matrix getY() {
		return y;
	}

	public Matrix getcB() {
		return cB;
	}

	public Matrix getcR() {
		return cR;
	}

	public int[][] getRed() {
		return red;
	}

	public int[][] getGreen() {
		return green;
	}

	public int[][] getBlue() {
		return blue;
	}
	
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public ColorTransform(BufferedImage buffer)
	{
		this.bImage = buffer;
		this.width = buffer.getWidth();
		this.height = buffer.getHeight();
		this.colorModel = buffer.getColorModel();
		
		this.red = new int [this.height] [this.width];
		this.green = new int [this.height] [this.width];
		this.blue = new int [this.height] [this.width];
		
		this.y = new Matrix(this.height, this.width);
		this.cB = new Matrix(this.height, this.width);
		this.cR = new Matrix(this.height, this.width);
	}
	
	public void getRGB()
	{
		for (int j = 0; j < this.height; j++) {
			for (int i = 0; i < this.width; i++) {
				this.red [j][i] = this.colorModel.getRed(this.bImage.getRGB(i, j));
				this.green [j][i] = this.colorModel.getGreen(this.bImage.getRGB(i, j));
				this.blue [j][i] = this.colorModel.getBlue(this.bImage.getRGB(i, j));
			}
		}
	}
	
	public ImagePlus setImageFromRGB(int height, int widht, int [][]red, int [][]green, int [][]blue)
	{
		BufferedImage bImage = new BufferedImage(widht, height, BufferedImage.TYPE_INT_RGB);
		int [][] rgb = new int [height][widht];
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < widht; j++) {
				rgb[i][j] = new Color(red[i][j], green[i][j], blue[i][j]).getRGB();
				bImage.setRGB(j, i, rgb[i][j]);
			}
			
		}
		
		return new ImagePlus("RGB", bImage);
	}

	public ImagePlus setImageFromRGB(int height, int widht, Matrix x, String component)
	{
		BufferedImage bImage = new BufferedImage(widht, height, BufferedImage.TYPE_INT_RGB);
		int [][] c = new int [height][widht];
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < widht; j++) {
//				if (x.get(i, j) > 255) {
//					x.set(i, j, 255);
//				} else if (x.get(i, j) < 0) {
//					x.set(i, j, 0);
//				}

				c[i][j] =  new Color((int)x.get(i, j),(int) x.get(i, j), (int)x.get(i, j)).getRGB();
				bImage.setRGB(j, i, c[i][j]);
			}
			
		}
		return new ImagePlus(component, bImage);
	}
	
	public void RGBtoYCbCr()
	{
		for (int i = 0; i < this.height; i++) {
			for (int j = 0; j < this.width; j++) {
				this.y.set(i, j, (0.257*this.red[i][j] + 0.504*this.green[i][j] + 0.098*this.blue[i][j] + 16));
				this.cB.set(i, j, (-0.148*this.red[i][j] - 0.291*this.green[i][j] + 0.439*this.blue[i][j] + 128));
				this.cR.set(i, j, (0.439*this.red[i][j] - 0.368*this.green[i][j] - 0.071*this.blue[i][j] + 128));
			}
		}
	}
	
	public void YCbCrtoRGB()
	{
		int r, g, b;
		for (int i = 0; i < this.height; i++) {
			for (int j = 0; j < this.width; j++) {
				
				r = (int) Math.round((1.164*(this.y.get(i, j) - 16) + 1.596*(this.cR.get(i, j) - 128)));
				g = (int) Math.round((1.164*(this.y.get(i, j) - 16) - 0.813*(this.cR.get(i, j) - 128) - 0.391*(this.cB.get(i, j) - 128)));
				b = (int) Math.round((1.164*(this.y.get(i, j) - 16) + 2.018*(this.cB.get(i, j) - 128)));
				
				if (r < 0) r = 0;
				else if (r > 255) r = 255;
				else this.red[i][j] = r;
				
				if (g < 0) g = 0;
				else if (g > 255) g = 255;
				else this.green[i][j] = g;
				
				if (b < 0) b = 0;
				else if (b > 255) b = 255;
				else this.blue[i][j] = b;
				
			}
		}
	}

	public Matrix transform (int size, Matrix transformMatrix, Matrix inputMatrix) {
		return transformMatrix.times(inputMatrix).times(transformMatrix.transpose());
	}

	public Matrix inverseTransform (int size, Matrix transformMatrix, Matrix inputMatrix) {
		return transformMatrix.transpose().times(inputMatrix).times(transformMatrix);
	}
}
