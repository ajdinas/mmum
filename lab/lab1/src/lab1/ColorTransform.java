package lab1;

import Jama.Matrix;
import ij.ImagePlus;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

public class ColorTransform {
    private int[][] red;
    private int[][] green;
    private int[][] blue;
    private int imageWidth;
    private int imageHeight;
    private Matrix y;
    private Matrix cb;
    private Matrix cr;
    private BufferedImage bImage;
    private ColorModel colorModel;

    public ColorTransform(BufferedImage bufferedImage) {
        this.bImage = bufferedImage;
        this.colorModel = this.bImage.getColorModel();
        this.imageWidth = this.bImage.getWidth();
        this.imageHeight = this.bImage.getHeight();
        this.red = new int[this.imageHeight][this.imageWidth];
        this.green = new int[this.imageHeight][this.imageWidth];
        this.blue = new int[this.imageHeight][this.imageWidth];
        this.y = new Matrix(this.imageHeight, this.imageWidth);
        this.cb = new Matrix(this.imageHeight, this.imageWidth);
        this.cr = new Matrix(this.imageHeight, this.imageWidth);
    }

    public void getRGB() {
        for(int i = 0; i < this.imageHeight; ++i) {
            for(int j = 0; j < this.imageWidth; ++j) {
                this.red[i][j] = this.colorModel.getRed(this.bImage.getRGB(j, i));
                this.green[i][j] = this.colorModel.getGreen(this.bImage.getRGB(j, i));
                this.blue[i][j] = this.colorModel.getBlue(this.bImage.getRGB(j, i));
            }
        }

    }

    public ImagePlus setImageFromRGB(int width, int height, int[][] r, int[][] g, int[][] b) {
        BufferedImage bImage = new BufferedImage(width, height, 1);
        int[][] rgb = new int[height][width];

        for(int i = 0; i < height; ++i) {
            for(int j = 0; j < height; ++j) {
                rgb[i][j] = (new Color(r[i][j], g[i][j], b[i][j])).getRGB();
                bImage.setRGB(j, i, rgb[i][j]);
            }
        }

        return new ImagePlus("RGB", bImage);
    }

    public int[][] getRed() {
        return this.red;
    }

    public int[][] getGreen() {
        return this.green;
    }

    public int[][] getBlue() {
        return this.blue;
    }

    public ImagePlus RGBtoYCbCr() {
        for(int i = 0; i < this.imageHeight; ++i) {
            for(int j = 0; j < this.imageWidth; ++j) {
                double y = 0.257D * (double)this.getRed()[i][j] + 0.504D * (double)this.getGreen()[i][j] + 0.098D * (double)this.blue[i][j] + 16.0D;
                this.y.set(i, j, y);
                double cb = -0.148D * (double)this.getRed()[i][j] - 0.291D * (double)this.getGreen()[i][j] + 0.439D * (double)this.blue[i][j] + 128.0D;
                this.cb.set(i, j, cb);
                double cr = 0.439D * (double)this.getRed()[i][j] - 0.368D * (double)this.getGreen()[i][j] - 0.071D * (double)this.blue[i][j] + 128.0D;
                this.cr.set(i, j, cr);
            }
        }

        return new ImagePlus("RGBtoYCbCr", this.bImage);
    }

    public ImagePlus YCbCrToRGB() {
        for(int i = 0; i < this.imageHeight; ++i) {
            for(int j = 0; j < this.imageWidth; ++j) {
                int red = (int)Math.round(1.164D * (this.y.get(i, j) - 16.0D) + 1.596D * (this.cr.get(i, j) - 128.0D));
                if (red > 255) {
                    red = 255;
                } else if (red < 0) {
                    red = 0;
                }

                this.red[i][j] = red;
                int green = (int)Math.round(1.164D * (this.y.get(i, j) - 16.0D) - 0.813D * (this.cr.get(i, j) - 128.0D) - 0.391D * (this.cb.get(i, j) - 128.0D));
                if (green > 255) {
                    green = 255;
                } else if (green < 0) {
                    green = 0;
                }

                this.green[i][j] = green;
                int blue = (int)Math.round(1.164D * (this.y.get(i, j) - 16.0D) + 2.018D * (this.cb.get(i, j) - 128.0D));
                if (blue > 255) {
                    blue = 255;
                } else if (blue < 0) {
                    blue = 0;
                }

                this.blue[i][j] = blue;
            }
        }

        return new ImagePlus("YCbCrtoRGB", this.bImage);
    }
}