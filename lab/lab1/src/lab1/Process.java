package lab1;

import ij.ImagePlus;

public class Process {
    private ImagePlus imagePlus;
    private ColorTransform colorTransform;

    public Process(ImagePlus imagePlus) {
        this.imagePlus = imagePlus;
        this.colorTransform = new ColorTransform(imagePlus.getBufferedImage());
        this.test1();
    }

    public void test1() {
        this.imagePlus.show("Original image");
        this.colorTransform.getRGB();
        this.colorTransform.RGBtoYCbCr();
        this.colorTransform.YCbCrToRGB();
        this.colorTransform.setImageFromRGB(this.colorTransform.getRed().length, this.colorTransform.getRed()[0].length, this.colorTransform.getRed(), this.colorTransform.getGreen(), this.colorTransform.getBlue()).show("Transformed Image");
    }
}