package lab1;

import ij.ImagePlus;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public Main() {
    }

    public void start(Stage primaryStage) {
        ImagePlus originalImagePlus = new ImagePlus("lena_std.jpg");
        new Process(originalImagePlus);
    }

    public static void main(String[] args) {
        launch(args);
    }
}