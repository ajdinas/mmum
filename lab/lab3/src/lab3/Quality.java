package lab3;

public class Quality {

    public double getMse(int[][] original, int[][] edited) {
        int imageWidth = original.length;
        int imageHeight = original[0].length;

        double mse = 1.0 / (imageWidth * imageHeight);
        double suma = 0;

        for (int i = 0; i < imageHeight; i++) {
            for (int j = 0; j < imageWidth; j++) {
                suma += (original[i][j] - edited[i][j]) * (original[i][j] - edited[i][j]);
            }
        }

        mse *= suma;

        return mse;
    }

    public double getPsnr (int[][] original, int[][] edited) {
        double mse = getMse(original, edited);
        // 10 * log10 ((2^8 - 1)^2 / mse)
        return 10 * Math.log10(Math.pow(Math.pow(2, 8) - 1, 2) / mse);
    }

}
