package lab2;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller extends Process {

    @FXML
    public Button rButton;

    @FXML
    public Button gButton;

    @FXML
    public Button bButton;

    @FXML
    public Button yButton;

    @FXML
    public Button cbButton;

    @FXML
    public Button crButton;

    @FXML
    public Button sample444;

    @FXML
    public Button sample422;

    @FXML
    public Button sample420;

    @FXML
    public Button sample411;

    @FXML
    public void rButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        getComponent(Process.RED).show("Red Component");
    }

    @FXML
    public void gButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        getComponent(Process.GREEN).show("Green Component");
    }

    @FXML
    public void bButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        getComponent(Process.BLUE).show("Blue Component");
    }

    @FXML
    public void yButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        getComponent(Process.Y).show("Y Component");
    }

    @FXML
    public void cbButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        getComponent(Process.CB).show("Cb Component");
    }

    @FXML
    public void crButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        getComponent(Process.CR).show("Cr Component");
    }

    @FXML
    public void sample444ButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        downSample(S444);
    }

    @FXML
    public void sample422ButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        downSample(S422);
    }

    @FXML
    public void sample420ButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        downSample(S420);
    }

    @FXML
    public void sample411ButtonPressed(ActionEvent event) {
        nactiOrigObraz();
        downSample(S411);
    }
}
