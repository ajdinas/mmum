package lab2;

import Jama.Matrix;
import ij.ImagePlus;
import javafx.fxml.Initializable;
import java.net.URL;
import java.util.ResourceBundle;

public class Process implements Initializable {

    public static final int RED = 68;
    public static final int GREEN = 39;
    public static final int BLUE = 55;
    public static final int Y = 33;
    public static final int CB = 22;
    public static final int CR = 44;
    public static final int S444 = 11;
    public static final int S422 = 12;
    public static final int S420 = 13;
    public static final int S411 = 14;

    private ImagePlus imagePlus;
    private ColorTransform colorTransform;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.nactiOrigObraz();
        this.colorTransform.RGBtoYCbCr();
        this.imagePlus.setTitle("Original Image");
        this.imagePlus.show("Original Image");
    }

    public void nactiOrigObraz() {
        this.imagePlus = new ImagePlus("lena_std.jpg");
        this.colorTransform = new ColorTransform(this.imagePlus.getBufferedImage());
        this.colorTransform.getRGB();
        this.colorTransform.RGBtoYCbCr();
    }

    public ImagePlus getComponent(int component) {
        ImagePlus imagePlus = null;

        switch (component) {
            case RED:
                imagePlus = colorTransform.setImageFromRGB(colorTransform.getImageWidth(), colorTransform.getImageHeight(), colorTransform.getRed(), "RED");
                break;
            case GREEN:
                imagePlus = colorTransform.setImageFromRGB(colorTransform.getImageWidth(), colorTransform.getImageHeight(), colorTransform.getGreen(), "GREEN");
                break;
            case BLUE:
                imagePlus = colorTransform.setImageFromRGB(colorTransform.getImageWidth(), colorTransform.getImageHeight(), colorTransform.getBlue(), "BLUE");
                break;
            case Y:
                imagePlus = colorTransform.setImageFromRGB(colorTransform.getImageWidth(), colorTransform.getImageHeight(), colorTransform.getY(), "Y");
                break;
            case CB:
                imagePlus = colorTransform.setImageFromRGB(colorTransform.getCb().getColumnDimension(), colorTransform.getCb().getRowDimension(), colorTransform.getCb(), "Cb");
                break;
            case CR:
                imagePlus = colorTransform.setImageFromRGB(colorTransform.getCr().getColumnDimension(), colorTransform.getCr().getRowDimension(), colorTransform.getCr(), "Cr");
                break;
        }

        return imagePlus;
    }

    public void downSample(int downsampleType) {
        Matrix cb = new Matrix(colorTransform.getCb().getArray());
        Matrix cr = new Matrix(colorTransform.getCr().getArray());

        getComponent(Process.Y).show("Y Component");

        switch (downsampleType) {
            case S444:
                colorTransform.setCb(cb);
                colorTransform.setCr(cr);
                break;
            case S422:
                colorTransform.setCb(colorTransform.downSample(cb));
                colorTransform.setCr(colorTransform.downSample(cr));
                break;
            case S420:
                colorTransform.setCb(colorTransform.downSample(colorTransform.downSample(cb).transpose()).transpose());
                colorTransform.setCr(colorTransform.downSample(colorTransform.downSample(cr).transpose()).transpose());
                break;
            case S411:
                colorTransform.setCb(colorTransform.downSample(colorTransform.downSample(cb)));
                colorTransform.setCr(colorTransform.downSample(colorTransform.downSample(cr)));
                break;
        }

        getComponent(CB).show("Cb Component");
        getComponent(CR).show("Cr Component");
    }
}