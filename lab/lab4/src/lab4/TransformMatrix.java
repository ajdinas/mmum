package lab4;

import Jama.Matrix;

public class TransformMatrix {

    public Matrix getDctMatrix (int size) {

        Matrix dct = new Matrix(size, size);
        double temp = 0.0;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == 0)
                    temp = Math.sqrt((double)1/size) * Math.cos(((2*j + 1) * i * 3.14) / (2 * size));
                else
                    temp = Math.sqrt((double)2/size) * Math.cos(((2*j + 1) * i * 3.14) / (2 * size));
                dct.set(i, j, temp);
            }
        }

        return dct;
    }

    public Matrix getWhtMatrix (int size) {

        double[][] array = new double[size][size];
        array[0][0] = 1;

        for (int n = 1; n < size; n += n) {
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) {
                    array[i + n][j] = array[i][j];
                    array[i][j + n] = array[i][j];
                    array[i + n][j + n] = (-1) * array[i][j];
                }
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

        Matrix result = new Matrix(size, size);

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result.set(i, j, array[i][j]);
            }
        }

        return result.times(1.0/(Math.sqrt(size)));
    }
}
