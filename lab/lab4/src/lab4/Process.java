package lab4;

import java.net.URL;
import java.util.ResourceBundle;

import Jama.Matrix;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import ij.ImagePlus;

public class Process implements Initializable{
	
	private ImagePlus imp;
	private ColorTransform colorTransform;
	private ColorTransform colorTransformOrig;

	public static final int RED = 1;
	public static final int GREEN = 2;
	public static final int BLUE = 3;
	
	public static final int Y = 4;
	public static final int Cb = 5;
	public static final int Cr = 6;
	
	public static final int S444 = 7;
	public static final int S422 = 8;
	public static final int S420 = 9;
	public static final int S411 = 10;

	public int downSampleType = S444;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
	
	public Process()
	{
		nactiObraz();
		colorTransform.RGBtoYCbCr();
		colorTransformOrig.RGBtoYCbCr();
		this.imp.show();
	}
	
	private void nactiObraz()
	{
		this.imp = new ImagePlus("lena_std.jpg");
		this.colorTransformOrig = new ColorTransform(this.imp.getBufferedImage());
		this.colorTransform = new ColorTransform(this.imp.getBufferedImage());
		colorTransform.getRGB();
		colorTransformOrig.getRGB();
	}

	@FXML
	public TextField field1;

	@FXML
	public TextField field2;

	public void rButtonPressed(ActionEvent event)
	{
		getComponent(RED).show();
	}
	
	public void gButtonPressed(ActionEvent event)
	{
		getComponent(GREEN).show();
	}
	
	public void bButtonPressed(ActionEvent event)
	{
		getComponent(BLUE).show();
	}
	
	public void yButtonPressed(ActionEvent event)
	{
		getComponent(Y).show();
	}
	
	public void CbButtonPressed(ActionEvent event)
	{
		getComponent(Cb).show();
	}
	
	public void CrButtonPressed(ActionEvent event)
	{
		getComponent(Cr).show();
	}
	
	public void S444ButtonPressed(ActionEvent event)
	{
		downSample(S444);
	}
	
	public void S422ButtonPressed(ActionEvent event)
	{
		downSample(S422);
	}
	
	public void S411ButtonPressed(ActionEvent event)
	{
		downSample(S411);
	}

	public void S420ButtonPressed(ActionEvent event)
	{
		downSample(S420);
	}

	public void overSampleButtonPressed(ActionEvent event)
	{
		overSample();
	}

	public void qualityButtonPressed(ActionEvent event)
	{
		quality();
	}

	public void dctButtonPressed(ActionEvent event) { dct(); }

	public void idctButtonPressed(ActionEvent event) { idct(); }

	public void whtButtonPressed(ActionEvent event)
	{
		wht();
	}

	public void iwhtButtonPressed(ActionEvent event)
	{
		iwht();
	}

	public ImagePlus getComponent (int component)
	{
		
		ImagePlus image = null;
		switch (component) {
		case RED:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getRed(), "Red");
			break;
			
		case GREEN:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getGreen(), "Green");
			break;
			
		case BLUE:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getBlue(), "Blue");
			break;
			
		case Y:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getY(), "Y");
			break;
			
		case Cb:
			image = this.colorTransform.setImageFromRGB(colorTransform.getcB().getRowDimension(), colorTransform.getcB().getColumnDimension(), this.colorTransform.getcB(), "Cb");
			break;
		
		case Cr:
			image = this.colorTransform.setImageFromRGB(colorTransform.getcR().getRowDimension(), colorTransform.getcR().getColumnDimension(), this.colorTransform.getcR(), "Cr");
			break;

		}
		return image;
	}
	
	public void downSample(int mode)
	{
		this.downSampleType = mode;

		if (mode != S444) {
			Matrix Cb = new Matrix(this.colorTransformOrig.getcB().getArray());
			Matrix Cr =new Matrix(this.colorTransformOrig.getcR().getArray());
			
				switch (mode) {
				case S422:
					  Cb = this.colorTransform.downSample(Cb);
					  Cr = this.colorTransform.downSample(Cr);
					break;
					
				case S411:
	
					for (int i = 0; i < 2; i++) 
					{
					  Cb = this.colorTransform.downSample(Cb);
					  Cr = this.colorTransform.downSample(Cr);
					}
					break;
					
				case S420:
					for (int i = 0; i < 2; i++) 
					{
						Cb = this.colorTransform.downSample(Cb);
						Cr = this.colorTransform.downSample(Cr);
						Cb = Cb.transpose();
						Cr = Cr.transpose();
					}
					break;
				}
				this.colorTransform.setcB(Cb);
				this.colorTransform.setcR(Cr);
			}
		
		this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getY(), "Y").show();
		this.colorTransform.setImageFromRGB(this.colorTransform.getcB().getRowDimension(), this.colorTransform.getcB().getColumnDimension(), this.colorTransform.getcB(), "Cb").show();
		this.colorTransform.setImageFromRGB(this.colorTransform.getcR().getRowDimension(), this.colorTransform.getcR().getColumnDimension(), this.colorTransform.getcR(), "Cr").show();
	}

	public void overSample() {

		Matrix cb = new Matrix(colorTransform.getcB().getArray());
		Matrix cr = new Matrix(colorTransform.getcR().getArray());

		getComponent(Process.Y).show("Y Component");

		switch (this.downSampleType) {
			case S444:
				colorTransform.setcB(cb);
				colorTransform.setcR(cr);
				break;
			case S422:
				colorTransform.setcB(colorTransform.overSample(cb));
				colorTransform.setcR(colorTransform.overSample(cr));
				break;
			case S420:
				colorTransform.setcB(colorTransform.overSample(colorTransform.overSample(cb).transpose()).transpose());
				colorTransform.setcR(colorTransform.overSample(colorTransform.overSample(cr).transpose()).transpose());
				break;
			case S411:
				colorTransform.setcB(colorTransform.overSample(colorTransform.overSample(cb)));
				colorTransform.setcR(colorTransform.overSample(colorTransform.overSample(cr)));
				break;
		}

		getComponent(Cb).show("Cb Component");
		getComponent(Cr).show("Cr Component");
	}

	public void quality() {
		this.colorTransform.YCbCrtoRGB();

		int[][] original = null;
		int[][] edited = null;

		Quality q = new Quality();

		// red
		original = this.colorTransformOrig.getRed();
		edited = this.colorTransform.getRed();
		double redMse = q.getMse(original, edited);
		double redPsnr = q.getPsnr(original, edited);

		// green
		original = this.colorTransformOrig.getGreen();
		edited = this.colorTransform.getGreen();
		double greenMse = q.getMse(original, edited);
		double greenPsnr = q.getPsnr(original, edited);

		// blue
		original = this.colorTransformOrig.getBlue();
		edited = this.colorTransform.getBlue();
		double blueMse = q.getMse(original, edited);
		double bluePsnr = q.getPsnr(original, edited);

		double mse = (redMse + greenMse + blueMse) / 3;
		System.out.println("MSE: " + mse);
		this.field1.setText("" + mse);


		double psnr = (redPsnr + greenPsnr + bluePsnr) / 3;
		System.out.println("PSNR: " + psnr);
		this.field2.setText("" + psnr);
	}

	public void dct() {
		TransformMatrix tm = new TransformMatrix();
		Matrix transformMatrix = tm.getDctMatrix(512);

		// transform
		Matrix y = this.colorTransform.transform(512, transformMatrix, this.colorTransform.getY());
		this.colorTransform.setY(y);

		Matrix cB = this.colorTransform.transform(512, transformMatrix, this.colorTransform.getcB());
		this.colorTransform.setcB(cB);

		Matrix cR = this.colorTransform.transform(512, transformMatrix, this.colorTransform.getcR());
		this.colorTransform.setcR(cR);
	}

	public void idct() {
		TransformMatrix tm = new TransformMatrix();
		Matrix transformMatrix = tm.getDctMatrix(512);

		// inverse transform
		Matrix y = this.colorTransform.inverseTransform(512, transformMatrix, this.colorTransform.getY());
		this.colorTransform.setY(y);

		Matrix cB = this.colorTransform.inverseTransform(512, transformMatrix, this.colorTransform.getcB());
		this.colorTransform.setcB(cB);

		Matrix cR = this.colorTransform.inverseTransform(512, transformMatrix, this.colorTransform.getcR());
		this.colorTransform.setcR(cR);

		this.colorTransform.YCbCrtoRGB();
		getComponent(Y).show("Y");
		getComponent(Cb).show("Cb");
		getComponent(Cr).show("Cr");
	}

	public void wht() {
		TransformMatrix tm = new TransformMatrix();
		Matrix transformMatrix = tm.getWhtMatrix(512);

		// transform
		Matrix y = this.colorTransform.transform(512, transformMatrix, this.colorTransform.getY());
		this.colorTransform.setY(y);

		Matrix cB = this.colorTransform.transform(512, transformMatrix, this.colorTransform.getcB());
		this.colorTransform.setcB(cB);

		Matrix cR = this.colorTransform.transform(512, transformMatrix, this.colorTransform.getcR());
		this.colorTransform.setcR(cR);
	}

	public void iwht() {
		TransformMatrix tm = new TransformMatrix();
		Matrix transformMatrix = tm.getWhtMatrix(512);

		// inverse transform
		Matrix y = this.colorTransform.inverseTransform(512, transformMatrix, this.colorTransform.getY());
		this.colorTransform.setY(y);

		Matrix cB = this.colorTransform.inverseTransform(512, transformMatrix, this.colorTransform.getcB());
		this.colorTransform.setcB(cB);

		Matrix cR = this.colorTransform.inverseTransform(512, transformMatrix, this.colorTransform.getcR());
		this.colorTransform.setcR(cR);

		this.colorTransform.YCbCrtoRGB();
		getComponent(Y).show("Y");
		getComponent(Cb).show("Cb");
		getComponent(Cr).show("Cr");
	}
}
