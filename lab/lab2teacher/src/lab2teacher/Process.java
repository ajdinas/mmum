package lab2teacher;

import java.net.URL;
import java.util.ResourceBundle;

import Jama.Matrix;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import ij.ImagePlus;

public class Process implements Initializable{
	
	private ImagePlus imp;
	private ColorTransform colorTransform;
	private ColorTransform colorTransformOrig;
	
	public static final int RED = 1;
	public static final int GREEN = 2;
	public static final int BLUE = 3;
	
	public static final int Y = 4;
	public static final int Cb = 5;
	public static final int Cr = 6;
	
	public static final int S444 = 7;
	public static final int S422 = 8;
	public static final int S420 = 9;
	public static final int S411 = 10;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
	
	public Process()
	{
		nactiObraz();
		colorTransform.RGBtoYCbCr();
		colorTransformOrig.RGBtoYCbCr();
		this.imp.show();
	}
	
	private void nactiObraz()
	{
		this.imp = new ImagePlus("lena_std.jpg");
		this.colorTransformOrig = new ColorTransform(this.imp.getBufferedImage());
		this.colorTransform = new ColorTransform(this.imp.getBufferedImage());
		colorTransform.getRGB();
		colorTransformOrig.getRGB();
	}
	
	public void rButtonPressed(ActionEvent event)
	{
		getComponent(RED).show();
	}
	
	public void gButtonPressed(ActionEvent event)
	{
		getComponent(GREEN).show();
	}
	
	public void bButtonPressed(ActionEvent event)
	{
		getComponent(BLUE).show();
	}
	
	public void yButtonPressed(ActionEvent event)
	{
		getComponent(Y).show();
	}
	
	public void CbButtonPressed(ActionEvent event)
	{
		getComponent(Cb).show();
	}
	
	public void CrButtonPressed(ActionEvent event)
	{
		getComponent(Cr).show();
	}
	
	public void S444ButtonPressed(ActionEvent event)
	{
		downSample(S444);
	}
	
	public void S422ButtonPressed(ActionEvent event)
	{
		downSample(S422);
	}
	
	public void S411ButtonPressed(ActionEvent event)
	{
		downSample(S411);
	}
	
	public void S420ButtonPressed(ActionEvent event)
	{
		downSample(S420);
	}
	
	public ImagePlus getComponent (int component)
	{
		
		ImagePlus image = null;
		switch (component) {
		case RED:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getRed(), "Red");
			break;
			
		case GREEN:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getGreen(), "Green");
			break;
			
		case BLUE:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getBlue(), "Blue");
			break;
			
		case Y:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getY(), "Y");
			break;
			
		case Cb:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getcB(), "Cb");
			break;
		
		case Cr:
			image = this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getcR(), "Cr");
			break;

		}
		return image;
	}
	
	public void downSample(int mode)
	{
		if (mode != S444) {
			Matrix Cb = new Matrix(this.colorTransformOrig.getcB().getArray());
			Matrix Cr =new Matrix(this.colorTransformOrig.getcR().getArray());
			
				switch (mode) {
				case S422:
					  Cb = this.colorTransform.downSample(Cb);
					  Cr = this.colorTransform.downSample(Cr);
					break;
					
				case S411:
	
					for (int i = 0; i < 2; i++) 
					{
					  Cb = this.colorTransform.downSample(Cb);
					  Cr = this.colorTransform.downSample(Cr);
					}
					break;
					
				case S420:
					for (int i = 0; i < 2; i++) 
					{
						Cb = this.colorTransform.downSample(Cb);
						Cr = this.colorTransform.downSample(Cr);
						Cb = Cb.transpose();
						Cr = Cr.transpose();
					}
					break;
				}
				this.colorTransform.setcB(Cb);
				this.colorTransform.setcR(Cr);
			}
		
		this.colorTransform.setImageFromRGB(this.colorTransform.getHeight(), this.colorTransform.getWidth(), this.colorTransform.getY(), "Y").show();
		this.colorTransform.setImageFromRGB(this.colorTransform.getcB().getRowDimension(), this.colorTransform.getcB().getColumnDimension(), this.colorTransform.getcB(), "Cb").show();
		this.colorTransform.setImageFromRGB(this.colorTransform.getcR().getRowDimension(), this.colorTransform.getcR().getColumnDimension(), this.colorTransform.getcR(), "Cr").show();
	}
	


}
